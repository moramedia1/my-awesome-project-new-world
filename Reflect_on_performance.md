# Reflect on your performance

I think the most important that has happened to me this year is how I have gained more confidence in my work and my relation with my peers and stakeholders. This has enabled me to go further on what's expected from me.

I can get bored easily when I'm doing the exact same tasks, but I have used that as an strength, helping others, ideating and experimenting on how to make the process better and creating value for myself.

This year I have been building a stronger relationship with a lot of coworkers. I'm very happy to collaborate with everyone and provide value to their job and careers.

I know there's a lot of room for improvement, I'm more aware of my weakness and how to tackle them in order to balance my strengths and skills. I'm working to improve in my involvement with the team and initiatives. I'm making that one of the most important priorities in this coming fiscal year.



I need to take a step further in participating in our team's initiatives and generate more value to our team.

I want to be considered a knowledgable person about the topics I am interested in and I need to make sure everyone knows that I'm open to present, teach and discuss those topics.

I want to be more in communication with my leaders and open to discuss about my performance and how I can help other people to grow their skills or career.

I will be working on having more and better information about trainings and certifications and I'll be happy to see people taking the certifications I researched.

I want to take courses or certifications myself. I'm very interested in learning about design thinking, product design and formalize my education about Agile methodologies.
